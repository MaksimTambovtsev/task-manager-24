package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.service.IAuthService;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.field.PermissionException;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.LoginEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.PasswordEmptyException;
import ru.tsc.tambovtsev.tm.exception.system.AccessDeniedException;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;


public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(@Nullable final String login,
                      @Nullable final String password) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @Nullable final User user = userService.findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        if (user.getLocked()) throw new AccessDeniedException();
        if (user.getPasswordHash() == null) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new AccessDeniedException();
        userId = user.getId();
    }

    @NotNull
    @Override
    public User getUser() {
        @Nullable final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public void logout() {
        if (isAuth()) throw new AccessDeniedException();
        userId = null;
    }

    @Override
    @Nullable
    public User registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public String getUserId() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
