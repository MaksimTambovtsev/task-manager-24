package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

    @Nullable
    void clear(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

    @Nullable
    int getSize(@Nullable String userId);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
