package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

}
