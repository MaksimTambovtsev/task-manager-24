package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractEntity> {

    @Nullable
    M add(@Nullable M model);

    void clear();

    @Nullable
    List<M> findAll();

    @Nullable
    boolean existsById(@Nullable String id);

    @Nullable
    M findById(@Nullable String id);

    @Nullable
    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

    @Nullable
    List<M> findAll(@Nullable Comparator comparator);

    @Nullable
    List<M> findAll(@Nullable Sort sort);

    void removeAll(@Nullable Collection<M> collection);

}
