package ru.tsc.tambovtsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.service.IAuthService;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Nullable
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

}
